This chart deploys both the vSphere CPI (Cloud Provider Interface) manager for vSphere as well as the CSI (Cloud Storage Interface) drivers and optional storageClass.

It also deploys MetalLB, cert-manager, external-dns and Kong Ingress controller/API gateway.
