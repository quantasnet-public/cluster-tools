## Manual steps, new cluster
1. When creating a new cluster, select the `External` cloudprovider. This way all deployed nodes will get the required taints.
2. Merge the config below with the generated cluster config `kubelet` configuration (`Edit as YAML (Cluster options)`):
  ```
  kubelet:
    extra_binds:
      - '/csi:/csi:rshared'
      - '/var/lib/csi/sockets/pluginproxy/csi.vsphere.vmware.com:/var/lib/csi/sockets/pluginproxy/csi.vsphere.vmware.com:rshared'
  ```
3. Create you cluster.
4. Deploy the chart in the **`kube-system`** namespace. 